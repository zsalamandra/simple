package ru.zrcompany.simple.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

	@PostMapping("/add")
	public ResponseEntity<Integer> add(@NonNull Integer a, @NonNull Integer b) {
		return ResponseEntity.ok(a + b);
	}

	@PostMapping("/minus")
	public ResponseEntity<Integer> minus(@NonNull Integer a, @NonNull Integer b) {
		return ResponseEntity.ok(a - b);
	}
}
