FROM fabric8/java-alpine-openjdk11-jdk
COPY target/simple-*.jar /opt/app/app.jar
USER root
EXPOSE 8080/tcp
CMD ["java", "-jar", "/opt/app/app.jar"]
